<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="model.Message"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
	String n = request.getParameter("name");
	String m = (String)session.getAttribute("userName");
	System.out.println(n);
	System.out.println(m);
	if(m.equals(n) != true){
		response.sendRedirect("140.125.84.95:8080/hw/listMessageAction");
	}
%>
<html lang="zh-CN">
<head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Msg</title>
</head>
<body>
	<center>
		<h3>Welcome <i>${sessionScope.userName}</i>, you can leave a message!</h3>
		<h3><a href="/hw/listMessageAction">Back</a></h3>
		<h2>Modify Message</h2>
		
		<%
			String mid = request.getParameter("id");
			System.out.println(mid);
		%>
		
		<s:push value="messageList"/>
		<s:form action="updateMessageAction"  method="post" enctype="multipart/form-data">
			<input type="hidden" name="id" value='<%= request.getParameter("id") %>' />
		  Name:<%= session.getAttribute("userName") %><input type="hidden" name="name" value='<%= session.getAttribute("userName") %>'>
		  <br/><br/>
		  <s:textfield class="col-sm-12 col-form-label" name="title" label="Title" value="" />
		  <s:textarea name="message" label="Message" value="" cols="50" rows="5" />
		  	<input type="hidden" name="date" value="" />
		  	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type="file" multiple name="upload">
		  	<br/><br/>
		  <s:submit />
		</s:form>
		
	</center>
</body>
</html>