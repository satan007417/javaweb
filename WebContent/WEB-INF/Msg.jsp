<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
    
<%
	if(session.getAttribute("userName") == null){
		response.sendRedirect("140.125.84.95:8080/hw/login");
	}
%>
<html lang="zh-CN">
<head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Msg</title>
</head>
<body>
	<center>
		<h3>Welcome <i>${sessionScope.userName}</i>, you can leave a message!</h3>
		<h3><a href="/hw/login">Home</a></h3>
		<h2>Add Message</h2>
		
		<s:form action="addMessageAction" method="post" enctype="multipart/form-data" >
		  Name:<%= session.getAttribute("userName") %><input type="hidden" name="name" value='<%= session.getAttribute("userName") %>'>
		  <br/><br/>
		  <s:textfield class="col-sm-12 col-form-label" name="title" label="Title" value="" />
		  <s:textarea name="message" label="Message" value="" cols="50" rows="5" />
		  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<input type="file" multiple name="upload">
		  <br/><br/>
		  <s:submit />
		</s:form>
		
		<h2>All Messages</h2>
		<s:if test="messageList.size() > 0"/>
		<table border="1px" cellpadding="8px" class="table table-striped"">
		<tbody>
			<tr>
				<th>Name</th>
				<th>Title</th>
				<th>Message</th>
				<th>Created Date</th>
				<th>File</th>
				<th>Modify</th>
				<th>Delete</th>
				
			</tr>
			<s:iterator value="messageList" status="userStatus" var="m">
				<tr>
					<% %>
					<td><s:property value="name" /></td>
					<td><s:property value="title" /></td>
					<td><s:property value="message" /></td>
					<td><s:date name="date" format="dd/MM/yyyy" /></td>
					<td>
						
						<c:forEach items="${ihmap}" var="entry" varStatus="status">
							<c:if test="${entry.key== m.id}">
							    
							        
							        <c:forEach var="files" items="${entry.value}">
										<s:url action="download" var="fileDownload" escapeAmp="false">
											<s:param name="fileName">${files}</s:param>
										</s:url>
										
										<a href="<s:property value="#fileDownload" />">${files}</a>
										
										
									</c:forEach>
							</c:if>
						</c:forEach>
						
					</td>
								<td>
								<a href="<s:url action="updateMessageAction" > <s:param name="id" value="%{id}"></s:param> <s:param name="name" value="%{name}"></s:param> <s:param name="file" value="%{file}"></s:param> </s:url>">Modify</a>
								<!-- <a href="hw/updateMessageAction?id=<s:property value="id"/>">Modify</a> -->
								</td>
								<td>
								<a href="<s:url action="delMessageAction" > <s:param name="id" value="%{id}"></s:param> <s:param name="name" value="%{name}"></s:param> </s:url>"> Delete </a>
								</td>
								
				</tr>
			</s:iterator>
			</tbody>
		</table>
		
	</center>
</body>
</html>