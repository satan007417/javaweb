<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<link href="lib/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="lib/jQuery/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>
</head>
<body>
    <center>
        <h3>Welcome <i>${sessionScope.userName}</i>, you have logged in successfully!</h3>
        <%= session.getAttribute("userName") %>
        <br>
        <h3><a href="/hw/listMessageAction">MsgPage</a></h3>
        <h3><a href="/hw/logout">Logout</a></h3>
    </center>
</body>
</html>