package model;

import java.util.Date;

import javax.persistence.Column;

public class Message implements java.io.Serializable  {

	private static final long serialVersionUID = 1L;
	private int id;
	@Column(name = "name", nullable = false)
	private String name;
	private String title;
	private String message;
	private Date date;
	@Column(name = "file", nullable = false)
	private String file;
	
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
