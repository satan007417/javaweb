package action;

import java.util.*;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session;
import org.hibernate.Transaction;

import model.Message;
import db.HibernateListener;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings({ "rawtypes", "serial", "unused" })
public class MsgAction extends ActionSupport implements ModelDriven{
	
//	private Map<String, List<String>> sessionMap ;
	IdentityHashMap<String, List<String>> ihmap = new IdentityHashMap<>();
	public IdentityHashMap<String, List<String>> getIhmap() {
		return ihmap;
	}

	public void setIhmap(IdentityHashMap<String, List<String>> ihmap) {
		this.ihmap = ihmap;
	}

	Message message = new Message();
	List<Message> messageList = new ArrayList<Message>();
	
	private List<File> upload;
    private List<String> uploadContentType;
    private List<String> uploadFileName;
    String contentType;
    long contentLength;
    String contentDisposition;
    public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}

	public String getContentDisposition() {
		return contentDisposition;
	}

	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	private String fileName;
    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	private InputStream inputStream;

    public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public List<File> getUpload() {
        return upload;
    }

    public void setUpload(List<File> upload) {
        this.upload = upload;
    }

    public List<String> getUploadContentType() {
        return uploadContentType;
    }

    public void setUploadContentType(List<String> uploadContentType) {
        this.uploadContentType = uploadContentType;
    }

    public List<String> getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(List<String> uploadFileName) {
        this.uploadFileName = uploadFileName;
    }
	
	public String execute() throws Exception {
		return SUCCESS;
	}

	public Object getModel() {
		return message;
	}
	
	public List<Message> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}
	
	public void setMsginfo(Message message) {
		this.message = message;
	}
	public Message getMsginfo() {
		return message;
	}
	
	
	//save message
		@SuppressWarnings("unchecked")
		public String addMessage() throws Exception{
			
			String FL = null;
			try {
				//文件保存路径
		        String path = ServletActionContext.getServletContext().getRealPath("/upload");
		        File file = new File(path);
		        //不存在则创建
		        if(!file.exists()){
		            file.mkdir();
		        }
		        //循环将文件上传到指定路径
		        for(int i = 0; i< upload.size(); i++){
		            FileUtils.copyFile(upload.get(i), new File(file,uploadFileName.get(i)));
		        }
		        
		        System.out.println(uploadFileName);
		        FL = String.join(",,", uploadFileName);
		        System.out.println(FL);
		        
	        }catch(Exception e) {
	        	;
	        }

			try {
				//get hibernate session from the servlet context
				SessionFactory sessionFactory = 
			         (SessionFactory) ServletActionContext.getServletContext()
		                     .getAttribute(HibernateListener.KEY_NAME);

				Session session = sessionFactory.openSession();

				//save it
				message.setDate(new Date());
				message.setFile(FL);
				session.beginTransaction();
				session.save(message);
				session.getTransaction().commit();
			 
				//reload the customer list
				messageList = null;
				messageList = session.createQuery("from Message").list();
				
				for(int i=0;i<messageList.size();i++) {
					List<String> myList = new ArrayList<String>(Arrays.asList(messageList.get(i).getFile().split(",,")));
					myList.remove("null");
					String k = Integer.toString(messageList.get(i).getId());;
					ihmap.put(k,myList);
					System.out.println(messageList.get(i).getFile());
					System.out.println(myList);
//					messageList.get(i).getFile();
				}
				for (String keys : ihmap.keySet())
				{
				   System.out.println(keys + ":"+ ihmap.get(keys));
				}
				
				session.close();
				
	        }catch(Exception e) {
	        	;
	        }
			
			return SUCCESS;
		
		}
		
		//update message
		public String updateMessage() throws Exception{
			
			HttpServletRequest request = ServletActionContext.getRequest();
			
			SessionFactory sessionFactory = 
			         (SessionFactory) ServletActionContext.getServletContext()
		                     .getAttribute(HibernateListener.KEY_NAME);

				Session session = sessionFactory.openSession();
				session.beginTransaction();
				
				String mid = request.getParameter("id");
				int midd = Integer.parseInt(mid);
				
				String filen = request.getParameter("file");
			
				String FL = null;
				String FLn = null;
			try {
				//文件保存路径
		        String path = ServletActionContext.getServletContext().getRealPath("/upload");
		        File file = new File(path);
		        //不存在则创建
		        if(!file.exists()){
		            file.mkdir();
		        }
		        //循环将文件上传到指定路径
		        for(int i = 0; i< upload.size(); i++){
		            FileUtils.copyFile(upload.get(i), new File(file,uploadFileName.get(i)));
		        }
		        
		        System.out.println(uploadFileName);
		        FL = String.join(",,", uploadFileName);
		        System.out.println(FL);
		        FLn = filen+",,"+FL;
		        System.out.println(FLn);
		        
	        }catch(Exception e) {
	        	;
	        }	
				
			try {
//				Message message = new Message();
//				Message m = (Message) session.get(Message.class, request.getParameter("id"));
				message.setId(midd);
				System.out.println(message.getId());
				message.setName(message.getName());
				message.setTitle(message.getTitle());
//				System.out.println(message.getTitle());
				message.setMessage(message.getMessage());
				message.setDate(new Date());
				message.setFile(FLn);
//				System.out.println(message);
				session.update(message);
				session.getTransaction().commit();
//				tx.commit();
				
	        }catch(Exception e) {
	        	;
	        }
			session.close();
			return SUCCESS;
		
		}
		
		//delete message
				@SuppressWarnings("unchecked")
				public String delMessage() throws Exception{
					
					
					Configuration configuration= new Configuration();
			        Configuration configure=configuration.configure("hibernate.cfg.xml");
			        SessionFactory sessionFactory = configure.buildSessionFactory();
			        Session session =  sessionFactory.openSession();
			        Transaction tx=session.beginTransaction();
			        
			        HttpServletRequest request = ServletActionContext.getRequest();
					String mn = request.getParameter("name");
					ActionContext actionContext = ActionContext.getContext();
					Map ss = actionContext.getSession();
					System.out.println(ss.get("userName"));
					String ssn = (String) ss.get("userName");
					if(ssn.equals(mn) != true) {
						messageList = null;
						messageList = session.createQuery("from Message").list();
						
						for(int i=0;i<messageList.size();i++) {
							List<String> myList = new ArrayList<String>(Arrays.asList(messageList.get(i).getFile().split(",,")));
							myList.remove("null");
							String k = Integer.toString(messageList.get(i).getId());;
							ihmap.put(k,myList);
							System.out.println(messageList.get(i).getFile());
							System.out.println(myList);
//							messageList.get(i).getFile();
						}
						for (String keys : ihmap.keySet())
						{
						   System.out.println(keys + ":"+ ihmap.get(keys));
						}
						
						session.close();
						return SUCCESS;
					}
						
					try {
						
//						HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get( ServletActionContext.HTTP_REQUEST);
//						message = (Message) session.get(Message.class, request.getParameter("id"));
						message.setId(message.getId());
//						System.out.println(message);
						session.delete(message);
						tx.commit();
						
						
			        }catch(Exception e) {
			        	;
			        }
					messageList = null;
					messageList = session.createQuery("from Message").list();
					
					for(int i=0;i<messageList.size();i++) {
						List<String> myList = new ArrayList<String>(Arrays.asList(messageList.get(i).getFile().split(",,")));
						while (myList.remove(null));
						String k = Integer.toString(messageList.get(i).getId());;
						ihmap.put(k,myList);
						System.out.println(messageList.get(i).getFile());
						System.out.println(myList);
//						messageList.get(i).getFile();
					}
					for (String keys : ihmap.keySet())
					{
					   System.out.println(keys + ":"+ ihmap.get(keys));
					}
					
					session.close();
					
					return SUCCESS;
				
				}
		
		//list all messages
		@SuppressWarnings("unchecked")
		public String listMessage() throws Exception{
			
			
			try {
				//get hibernate session from the servlet context
				SessionFactory sessionFactory = 
			         (SessionFactory) ServletActionContext.getServletContext()
		                     .getAttribute(HibernateListener.KEY_NAME);

				Session ss = sessionFactory.openSession();

				messageList = ss.createQuery("from Message").list();
				System.out.println(messageList);
				
//				ArrayList listmap = new ArrayList();
				
//				HttpServletRequest request = ServletActionContext.getRequest();
				
				for(int i=0;i<messageList.size();i++) {
					List<String> myList = new ArrayList<String>(Arrays.asList(messageList.get(i).getFile().split(",,")));
					myList.remove("null");
					String k = Integer.toString(messageList.get(i).getId());;
					ihmap.put(k,myList);
					System.out.println(messageList.get(i).getFile());
					System.out.println(myList);
//					messageList.get(i).getFile();
				}
				for (String keys : ihmap.keySet())
				{
				   System.out.println(keys + ":"+ ihmap.get(keys));
				}
					
				ss.close();
				
	        }catch(Exception e) {
	        	;
	        }
			
			return SUCCESS;
		
		}
		
		
				 
		 //2.2：下载提交的业务方法（在struts.xml中配置返回stream）
		 public String down() throws Exception {
			 
			 HttpServletRequest request = ServletActionContext.getRequest();
			 fileName = request.getParameter("fileName");
			 
			 String path=ServletActionContext.getServletContext().getRealPath("/upload");
			 
			 ServletContext servletContext = ServletActionContext.getServletContext();
			// 獲取檔案的路徑
			// String realPath = servletContext.getRealPath("/WEB-INF/files/test.txt");
			String realPath = "D:\\eclipse-workspace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\hw\\upload\\" + fileName;
			// 獲取檔案的流
			File file = new File(realPath);
			inputStream = new FileInputStream(realPath);
			// inputStream =
			// ServletActionContext.getServletContext().getResourceAsStream(realPath);
			// 設定檔案的型別
			contentType = servletContext.getMimeType(realPath);
			System.out.println(contentType);
			// 獲取檔案的長度
			contentLength = new File(realPath).length();
			// 設定檔名
			// filename = new String(filename.getBytes("gbk"), "iso8859-1");
			contentDisposition = "attachment;filename=" + fileName;
			
			// System.out.println("D:\\1104410038\\新增資料夾111\\backpage\\" + url );
			// fileInputStream = new FileInputStream(new
			// File("D:\\1104410038\\新增資料夾111\\backpage\\" + url));
			 
		     return "download";
		 }
		 
		
		protected HttpServletResponse servletResponse;
		  public void setServletResponse(HttpServletResponse servletResponse) {
		    this.servletResponse = servletResponse;
		  }

		  protected HttpServletRequest servletRequest;
		  public void setServletRequest(HttpServletRequest servletRequest) {
		    this.servletRequest = servletRequest;
		  }
//		  public void setSession(Map<String, List<String>> sessionMap) {
//			  this.sessionMap = sessionMap;
//		    }
	
}
